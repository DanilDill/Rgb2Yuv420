//
// Created by ddd on 13.09.2023.
//

#ifndef RGB2YUV_RGBTOYUVFILTER_H
#define RGB2YUV_RGBTOYUVFILTER_H
#include <memory>
#include <vector>
#include "RGBFrame.h"

class RgbToYuvFilter
{
public:
    static   unsigned char Y(const unsigned char R,const unsigned char G,const unsigned char B);
    static  unsigned char U(const unsigned char R,const unsigned char G,const unsigned char B);
    static  unsigned char V(const unsigned char R,const unsigned char G,const unsigned char B);
   static std::shared_ptr<std::vector<unsigned char>> Y(const std::shared_ptr<RGBFrame>& frame);
   static std::shared_ptr<std::vector<unsigned char>> U(const std::shared_ptr<RGBFrame>& frame);
   static std::shared_ptr<std::vector<unsigned char>> V(const std::shared_ptr<RGBFrame>& frame);

};

#endif //RGB2YUV_RGBTOYUVFILTER_H
