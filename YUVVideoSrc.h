

#ifndef RGB2YUV_YUVVIDEOSRC_H
#define RGB2YUV_YUVVIDEOSRC_H
#include <fstream>
#include "ElementBase.h"
#include "YUV420Frame.h"
class YUVVideoSrc : public ElementBase<std::ifstream,YUV420Frame>
{
public:
    YUVVideoSrc();
    YUVVideoSrc(unsigned int Width, unsigned int Height);
    virtual void start()override;
protected:
    virtual  std::map<const std::string,std::string> _create_properties()override;
    virtual std::vector<std::string> _create_input_caps()override;
    virtual std::vector<std::string> _create_output_caps()override;
    virtual  std::string _create_id()override;

private:
    bool hasSize;
    bool isOpen;
    unsigned int Width;
    unsigned int Height;
private:
    void _openFile();
    void _getFrame();
    void getSizeFromProps();
};

bool is_number(const std::string& s);

#endif
