
#ifndef RGB2YUV_ELEMENTBASE_H
#define RGB2YUV_ELEMENTBASE_H
#include <vector>
#include <memory>
#include <string>
#include <map>
#include <algorithm>
#include <iostream>

template<typename Input,typename Output>
class ElementBase
{
public:
    ElementBase();
    const std::map<const std::string,std::string>& get_properties();
    const std::vector<std::string>& get_input_caps();
    const std::vector<std::string>& get_output_caps();
    void set_property(const std::string& name, const std::string& value);
    const std::string& get_property(const std::string& );
    virtual void setInputBuffer(std::shared_ptr<Input>){};
    void pull_up(const std::shared_ptr<Input>& data = nullptr){_inputBuffer = std::move(data);};
    std::shared_ptr<Output> push_down(){ return std::move(_outputBuffer);};
    virtual void start()=0;

protected:
    ElementBase(std::map<const std::string,std::string> props, const std::vector<std::string> in_caps,
                const std::vector<std::string> out_caps, const std::string id);

    std::string& _get_property(const std::string& );
    virtual  std::map<const std::string,std::string> _create_properties()=0;
    virtual std::vector<std::string> _create_input_caps()=0;
    virtual std::vector<std::string> _create_output_caps()=0;
    virtual  std::string _create_id()=0;


    std::string _id;
    std::map<const std::string,std::string> _properties;
    std::vector<std::string> _input_caps;
    std::vector<std::string> _output_caps;
    std::shared_ptr<Input> _inputBuffer;
    std::shared_ptr<Output> _outputBuffer;
};

template<typename Input,typename Output>
ElementBase<Input,Output>::ElementBase():_inputBuffer(nullptr),_outputBuffer(nullptr)
{
}
template<typename Input,typename Output>
ElementBase<Input,Output>::ElementBase(std::map<const std::string,std::string> props, const std::vector<std::string> in_caps,
            const std::vector<std::string> out_caps,const std::string id):
        _input_caps(std::move(in_caps)),_properties(std::move(props)), _output_caps(std::move(out_caps)),_id(std::move(id)),_inputBuffer(nullptr),_outputBuffer(nullptr){};

template<typename Input,typename Output>
const std::map<const std::string,std::string>&ElementBase<Input,Output>::get_properties()
{
    return _properties;
}

template<typename Input,typename Output>
const std::vector<std::string>& ElementBase<Input,Output>::get_input_caps()
{
    return _input_caps;
}

template<typename Input,typename Output>
const std::vector<std::string>& ElementBase<Input,Output>::get_output_caps()
{
    return _output_caps;
}

template<typename Input,typename Output>
void ElementBase<Input,Output>::set_property(const std::string& name, const std::string& value)
{
    _get_property(name) = value;
}

template<typename Input,typename Output>
const std::string& ElementBase<Input,Output>::get_property(const std::string& name)
{
    return _get_property(name);
}



template<typename Input,typename Output>
std::string& ElementBase<Input,Output>::_get_property(const std::string& name)
{
    try {
        auto search = _properties.find(name);
        if ( search != _properties.end())
        {
            return search->second;
        }
        else
        {
            throw std::string("Element has no property name \"" + _id + "\"");
        }
    }
    catch (std::string& error_message)
    {
        std::cerr << "\"Exeption occured. What:\"" << error_message << "\"" << std::endl;
    }
}

#endif
