#include <thread>
#include <future>
#include <fstream>
#include "YUVFrameMerger.h"

YUVFrameMerger::YUVFrameMerger():ElementBase(_create_properties(),_create_input_caps(),_create_output_caps(),_create_id()){}

void YUVFrameMerger::start()
{
    size_t width = _inputBuffer->first.Width*_inputBuffer->first.Height >= _inputBuffer->second.Width *_inputBuffer->second.Height ?
                   _inputBuffer->first.Width : _inputBuffer->second.Width;
    size_t height = _inputBuffer->first.Width*_inputBuffer->first.Height >= _inputBuffer->second.Width *_inputBuffer->second.Height ?
                    _inputBuffer->first.Height : _inputBuffer->second.Height;
     bool isAsync = get_property("async") == "true";
    if (isAsync)
    {
        auto Y = std::move(std::async(std::launch::async,[&](){ return std::move(merge(_inputBuffer->first.Y,
                                                                                                      _inputBuffer->first.Width,
                                                                                                      _inputBuffer->first.Height,
                                                                                                      _inputBuffer->second.Y,
                                                                                                      _inputBuffer->second.Width,
                                                                                                      _inputBuffer->second.Height));}).get());

        auto U = std::move(std::async(std::launch::async,[&](){ return std::move(merge(_inputBuffer->first.U,
                                                                                                _inputBuffer->first.Width/2,
                                                                                                      _inputBuffer->first.Height/2,
                                                                                                      _inputBuffer->second.U,
                                                                                                      _inputBuffer->second.Width/2,
                                                                                                      _inputBuffer->second.Height/2));}).get());

        auto V = std::move(std::async(std::launch::async,[&](){ return std::move(merge(_inputBuffer->first.U,
                                                                                               _inputBuffer->first.Width/2,
                                                                                                    _inputBuffer->first.Height/2,
                                                                                                    _inputBuffer->second.U,
                                                                                            _inputBuffer->second.Width/2,
                                                                                                    _inputBuffer->second.Height/2));}).get());
        _outputBuffer = std::move(std::make_shared<YUV420Frame>(YUV420Frame(width,height,Y,U,V)));
    } else
    {
        _outputBuffer = std::move(std::make_shared<YUV420Frame>(YUV420Frame(width,height,
                                                                            merge(_inputBuffer->first.Y,
                                                                                  _inputBuffer->first.Width,
                                                                                  _inputBuffer->first.Height,
                                                                                  _inputBuffer->second.Y,
                                                                                  _inputBuffer->second.Width,
                                                                                  _inputBuffer->second.Height),
                                                                            merge(_inputBuffer->first.U,
                                                                                  _inputBuffer->first.Width/2,
                                                                                  _inputBuffer->first.Height/2,
                                                                                  _inputBuffer->second.U,
                                                                                  _inputBuffer->second.Width/2,
                                                                                  _inputBuffer->second.Height/2),
                                                                            merge(_inputBuffer->first.U,
                                                                                  _inputBuffer->first.Width/2,
                                                                                  _inputBuffer->first.Height/2,
                                                                                  _inputBuffer->second.U,
                                                                                  _inputBuffer->second.Width/2,
                                                                                  _inputBuffer->second.Height/2))));
    }

}

std::shared_ptr<std::vector<unsigned char>> YUVFrameMerger::merge(const std::shared_ptr<std::vector<unsigned char >>& left,
                                                                  unsigned int left_w, unsigned int left_h,
                                                                  const std::shared_ptr<std::vector<unsigned char>>& right,
                                                                  unsigned int right_w,unsigned int right_h)
{
    size_t sizeMax = left->size() >= right->size() ? left->size() : right->size();
    size_t widthMax = left->size() >= right->size() ? left_w : right_w;
    size_t widthMin = left->size() <= right->size() ? left_w : right_w;
    size_t heightMax = left->size() >= right->size() ? left_h : right_h;
    size_t heightMin = left->size() <= right->size() ? left_h : right_h;
    const std::shared_ptr<std::vector<unsigned char >>& max =  left->size() >= right->size() ? left : right;
    const std::shared_ptr<std::vector<unsigned char >>& min =  left->size() <= right->size() ? left : right;
    std::shared_ptr<std::vector<unsigned char>> result = std::make_shared<std::vector<unsigned char>>(std::vector<unsigned char>(sizeMax));

    for (size_t i = 0; i < heightMax ; ++i)
    {
        for (size_t j = 0; j < widthMax; ++j)
        {

            if (j < widthMin && i < heightMin)
            {
                result->at(i* widthMax + j) = min->at(i* widthMin + j);
            } else
            {
                result->at(i* widthMax + j) = max->at(i * widthMax + j);
            }

        }
    }

    return result;

}

std::map<const std::string,std::string> YUVFrameMerger::_create_properties()
{
    return {
            {"async", "true"},
            {"name",""},
    };
}

std::vector<std::string> YUVFrameMerger::_create_input_caps()
{
    return  {{"image/yuv420"}};
}

std::vector<std::string> YUVFrameMerger::_create_output_caps()
{
    return  {{"image/yuv420"}};
}

std::string YUVFrameMerger::_create_id()
{
    return std::string("YUVFrameMerger");
}