
#ifndef RGB2YUV_RGB2YUV420CONVERTER_H
#define RGB2YUV_RGB2YUV420CONVERTER_H

#include "ElementBase.h"
#include "RGBFrame.h"
#include "YUV420Frame.h"

class Rgb2Yuv420Converter : public ElementBase<RGBFrame,YUV420Frame>
{
public:
    Rgb2Yuv420Converter();
    virtual void start() override;
protected:
    virtual  std::map<const std::string,std::string> _create_properties()override;
    virtual std::vector<std::string> _create_input_caps()override;
    virtual std::vector<std::string> _create_output_caps()override;
    virtual  std::string _create_id()override;

};


#endif
