//
// Created by ddd on 28.09.2023.
//

#ifndef RGB2YUV_YUVFRAMEMERGER_H
#define RGB2YUV_YUVFRAMEMERGER_H
#include "ElementBase.h"
#include "YUV420Frame.h"

class YUVFrameMerger : public ElementBase<std::pair<YUV420Frame,YUV420Frame>,YUV420Frame>
{
public:
    YUVFrameMerger();
    virtual void start()override;
protected:
    virtual  std::map<const std::string,std::string> _create_properties()override;
    virtual std::vector<std::string> _create_input_caps()override;
    virtual std::vector<std::string> _create_output_caps()override;
    virtual  std::string _create_id()override;
private:
    std::shared_ptr<std::vector<unsigned char>> merge(const std::shared_ptr<std::vector<unsigned char >>& left,
                                                      unsigned int left_w, unsigned int left_h,
                                                      const std::shared_ptr<std::vector<unsigned char>>& right,
                                                      unsigned int right_w,unsigned int right_h);
};


#endif
