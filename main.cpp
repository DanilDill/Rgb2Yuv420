
#include <iostream>
#include <algorithm>
#include "RGBFrame.h"
#include "YUV420Frame.h"
#include "BmpImageSrc.h"
#include "Rgb2Yuv420Converter.h"
#include "YUVFrameMerger.h"
#include "YUVVideoSrc.h"

std::vector<std::string> split(const std::string& stringToSplit,const std::string& delimiters )
{
    using namespace std;
    string::size_type start = stringToSplit.find_first_not_of(delimiters, 0);
    string::size_type stop = stringToSplit.find_first_of(delimiters, start);

    std::vector<std::string> tokens;
    while (string::npos != stop || string::npos != start) {
        tokens.push_back(stringToSplit.substr(start, stop - start));
        start = stringToSplit.find_first_not_of(delimiters, stop);
        stop = stringToSplit.find_first_of(delimiters, start);
    }
    return tokens;
}


int main(int argc, char* argv[])
{
    std::vector<std::string> all_args;
    if (argc > 1)
    {
        all_args.assign(argv + 1, argv + argc);
    }
   std::string inputBmpFilePath =  all_args[0];
   std::string inputVideoPath  = all_args[1];
   std::string wStr = split(all_args[2],std::string("*"))[0];
   std::string hStr = split(all_args[2],std::string("*"))[1];
   int width = 0;
   int height = 0;
  try
  {
      if (is_number(wStr) && is_number(hStr))
      {
          width =  std::stoi(wStr);
          height = std::stoi(hStr);
      } else
      {
          throw std::string ("Incorrect Width or Height");
      }
  }
  catch (std::string& er)
  {
      std::cerr << "Error ocurred. What: " << er;
  }

    std::shared_ptr<ElementBase<std::ifstream ,RGBFrame>> src = std::make_shared<BmpImageSrc>();
    src->set_property("location", inputBmpFilePath);
    std::shared_ptr<ElementBase<RGBFrame,YUV420Frame>> converter = std::make_shared<Rgb2Yuv420Converter>();
    src->start();
    converter->pull_up(src->push_down());
    converter->start();
    std::shared_ptr<ElementBase<std::pair<YUV420Frame,YUV420Frame>,YUV420Frame>> merger = std::make_shared<YUVFrameMerger>();
    std::shared_ptr<ElementBase<std::ifstream,YUV420Frame>> videosrc = std::make_shared<YUVVideoSrc>(352,288);
    videosrc->set_property("location",inputVideoPath);
    std::ofstream out("output_video.yuv", std::ios::binary);
    auto frame1 = converter->push_down();
    while (true)
    {
        videosrc->start();
        auto frame2 = videosrc->push_down();
        if (frame2)
        {
            auto pair = std::make_shared<std::pair<YUV420Frame,YUV420Frame>>(std::make_pair((*frame1),(*frame2)));
            merger->pull_up(pair);
            merger->start();
            out << *merger->push_down();
        }
        else
        {
            break;
        }

    }

    return 0;

}

