
#include "RGBFrame.h"
#include <cstring>
#include <fstream>
RGBFrame::RGBFrame(const unsigned int& size,unsigned long w,unsigned long h) :RGBFrame()
{
    sizeBytes = size;
    Width = w;
    Height = h;
    data = std::make_shared<std::vector<RGBPixel>>(std::vector<RGBPixel>(Width*Height));
}

unsigned int RGBFrame::ByteSize()
{
    return sizeof(RGBPixel) * data->size();
}

RGBPixel RGBFrame::operator[](const int i)
{
    return data->at(i);
}

size_t RGBFrame::size()
{
    return data->size();
}

std::shared_ptr<std::vector<RGBPixel>>& RGBFrame::getPixels()
{
    return data;
}

std::istream& operator>>(std::istream& stream, RGBFrame& rgbFrame)
{
   return stream.read(reinterpret_cast<char*>(rgbFrame.data->data()),rgbFrame.sizeBytes);
}
