//
// Created by ddd on 25.09.2023.
//

#ifndef RGB2YUV_YUV420FRAME_H
#define RGB2YUV_YUV420FRAME_H
#include <vector>
#include <memory>
#include "RGBFrame.h"
#include "RgbToYuvFilter.h"

class YUV420Frame
{
public:

    YUV420Frame(unsigned int w ,unsigned int h);
    YUV420Frame(const YUV420Frame& other);
    YUV420Frame(YUV420Frame&& other);
    YUV420Frame(unsigned int w ,unsigned int h, const std::shared_ptr<std::vector<unsigned char>>& Y,
                const std::shared_ptr<std::vector<unsigned char>>& U, const std::shared_ptr<std::vector<unsigned char>>& V);
    YUV420Frame& operator=(YUV420Frame&& other);
    YUV420Frame& operator=(const YUV420Frame& other);
    YUV420Frame(const std::shared_ptr<RGBFrame>& rgbFrame, bool async=false);
    YUV420Frame(unsigned int w ,unsigned int h, std::istream&);
    friend std::ofstream& operator<< (std::ofstream& out, YUV420Frame& frame);
    friend std::istream& operator>>(std::istream& stream, YUV420Frame& frame);
    friend class RgbToYuvFilter;
    friend class YUVFrameMerger;

    unsigned int Width;
    unsigned int Height;
private:

    YUV420Frame()=default;

    std::shared_ptr<std::vector<unsigned char>> Y;
    std::shared_ptr<std::vector<unsigned char>> U;
    std::shared_ptr<std::vector<unsigned char>> V;
};


#endif
