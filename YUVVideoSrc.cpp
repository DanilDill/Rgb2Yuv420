
#include "YUVVideoSrc.h"

YUVVideoSrc::YUVVideoSrc():ElementBase(_create_properties(),_create_input_caps(),_create_output_caps(),_create_id())
{
    hasSize = false;
    isOpen = false;
}

bool is_number(const std::string& s)
{
    return !s.empty() && std::find_if(s.begin(),
                                      s.end(), [](unsigned char c) { return !std::isdigit(c); }) == s.end();
}

YUVVideoSrc::YUVVideoSrc(unsigned int width, unsigned int height): YUVVideoSrc()
{
    Width = width;
    Height = height;
    hasSize = true;
}
void YUVVideoSrc::_getFrame()
{
    _outputBuffer = std::make_shared<YUV420Frame>(YUV420Frame(Width,Height,*_inputBuffer));
}
void YUVVideoSrc::getSizeFromProps()
{
    try
    {
        if (!is_number(get_property("Width"))  || is_number(get_property("Height")) )
        {
            throw std::string("Is not digit!");
        }
        Width = std::stoi(get_property("Width"));
        Height = std::stoi(get_property("Height"));
        hasSize = true;
    }
   catch (std::string& er)
   {
       std::cerr << "one or more parameter of " << _id << "has incorrect value: " << er << std::endl;
       std::terminate();

   }
}

void YUVVideoSrc::start()
{
    if (!hasSize)
    {
        getSizeFromProps();
    }
    if (!isOpen)
    {
        _openFile();
    }
    if (!_inputBuffer->eof())
    {
        _getFrame();
    }
    else
    {
        _outputBuffer= nullptr;
    }


}

void YUVVideoSrc::_openFile()
{
    std::shared_ptr<std::ifstream> inputFile = std::make_shared<std::ifstream>(std::ifstream
                                        (_get_property("location"),std::fstream::in | std::fstream::binary));
    try
    {
        if (!inputFile->is_open())
        {
            throw std::string("Failed to open file");
        }
        pull_up(inputFile);
        isOpen = true;
    }
    catch (const std::string& error_message)
    {
        std::cerr<<"Exeption occured. What:" << error_message;
        std::terminate();
    }
}

std::string YUVVideoSrc::_create_id()
{
    return "YUV420VideoSrc";
}
std::map<const std::string,std::string> YUVVideoSrc::_create_properties()
{
    return {
            {"location",""},
            {"Width",""},
            {"Height",""},
            {"name",""},
    };
}
std::vector<std::string> YUVVideoSrc::_create_input_caps()
{
    return {{""}};
}
std::vector<std::string> YUVVideoSrc::_create_output_caps()
{
    return {{"image/yuv"}};
}

