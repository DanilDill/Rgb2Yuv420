//
// Created by ddd on 25.09.2023.
//

#ifndef RGB2YUV_BMPIMAGESRC_H
#define RGB2YUV_BMPIMAGESRC_H
#include <fstream>
#include "ElementBase.h"
#include "RGBFrame.h"
class BmpImageSrc : public ElementBase<std::ifstream,RGBFrame>
{
public:
    BmpImageSrc();
    virtual void start() override;

protected:
    virtual  std::map<const std::string,std::string> _create_properties()override;
    virtual std::vector<std::string> _create_input_caps()override;
    virtual std::vector<std::string> _create_output_caps()override;
    virtual  std::string _create_id()override;

private:
    void _openFile();
    void _getHeaderInfo();
    void _getData();
private:
    unsigned long Width;
    unsigned long Height;
    unsigned long unDataBytes;



};


#endif
