
#include "Rgb2Yuv420Converter.h"

Rgb2Yuv420Converter::Rgb2Yuv420Converter():ElementBase(_create_properties(),_create_input_caps(),_create_output_caps(),_create_id())
{

}
void Rgb2Yuv420Converter::start()
{
    bool isAsync = get_property("async") == "true";
    _outputBuffer = std::make_shared<YUV420Frame>(_inputBuffer,isAsync);
    _inputBuffer.reset();
}


std::map<const std::string,std::string> Rgb2Yuv420Converter::_create_properties()
{
    return {
            {"async", "true"},
            {"name",""},
    };
}
std::vector<std::string> Rgb2Yuv420Converter::_create_input_caps()
{
    return {{"image/rgb"}};
}
std::vector<std::string> Rgb2Yuv420Converter::_create_output_caps()
{
    return  {{"image/yuv420"}};
}
std::string Rgb2Yuv420Converter::_create_id()
{
    return  "Rgb2YuvConverter";
}