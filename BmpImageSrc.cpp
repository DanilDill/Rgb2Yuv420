
#include "BmpImageSrc.h"
#include <iostream>
#pragma pack(push, 1)
struct BitmapFileHeader
{
    unsigned short bfType;
    uint32_t bfSize;
    uint32_t  bfReserved1;
    uint32_t bfReserved2;
    uint32_t bfOffBits;
    friend std::istream& operator>>(std::istream& stream, BitmapFileHeader& fileHeader)
    {
        return stream.read((char *)&fileHeader,sizeof(fileHeader));

    }
};

struct BitmapInfoHeader
{
    int32_t biWidth;
    int32_t biHeight;
    uint16_t planes;
    uint16_t bitsPerPixel;
    uint32_t biCompression;
    uint32_t dataSize;
    int32_t horizontalResolution;
    int32_t verticalResolution;
    uint32_t colors;
    uint32_t importantColors;

    friend std::istream& operator>>(std::istream& stream, BitmapInfoHeader& fileInfoHeader)
    {
        return stream.read(reinterpret_cast<char*>(&fileInfoHeader),sizeof(fileInfoHeader));
    }
};


BmpImageSrc::BmpImageSrc() :ElementBase(_create_properties(),_create_input_caps(),_create_output_caps(),_create_id())
{
}

void BmpImageSrc::_openFile()
{
    std::shared_ptr<std::ifstream>inputFile = std::make_shared<std::ifstream>(std::ifstream (_get_property("location"),std::fstream::in | std::fstream::binary));
    try
    {
        if (!inputFile->is_open())
        {
            throw std::string("Failed to open file");
        }
        pull_up(inputFile);
    }
    catch (const std::string& error_message)
    {
        std::cerr<<"Exeption occured. What:" << error_message;
        std::terminate();
    }
}
void BmpImageSrc::_getHeaderInfo()
{
    try
    {
        BitmapFileHeader bmpFileHeader;
        *_inputBuffer >> bmpFileHeader;


        if (static_cast<ushort>(bmpFileHeader.bfType) !=  static_cast<ushort>(0x4D42))  // "BM"
        {
            throw std::string("Invalid bitmap file type");
        }
        BitmapInfoHeader bmpFileInfoHeader;
        *_inputBuffer >> bmpFileInfoHeader;

        if (bmpFileInfoHeader.bitsPerPixel != 24)
        {
            throw std::string("Unsupported bitmap pixel format");
        }

        if (bmpFileInfoHeader.biCompression != 0x0000)
        {
            throw std::string("Unsupported bitmap compression");
        }
        Width = bmpFileInfoHeader.biWidth;
        Height = bmpFileInfoHeader.biHeight;
        if (Width % 4)
        {
            Width = (Width / 4 + 1) * 4;
        }
        unDataBytes = Width * 3 * abs(bmpFileInfoHeader.biHeight);

    }
    catch (std::string error_message)
    {
        std::cerr<<"Exeption occured. What:" << error_message ;
        std::terminate();
    }
}
void BmpImageSrc::_getData()
{
    try
    {
        _outputBuffer = std::make_shared<RGBFrame>(RGBFrame(unDataBytes,Width,Height));
        *_inputBuffer >> *_outputBuffer;
        size_t size = _outputBuffer->ByteSize();
        if ( size != unDataBytes)
        {
            throw std::string(" Unable to read RGB data");
        }
    }
    catch (std::string error_message)
    {
        std::cerr<<"Exeption occured. What:" << error_message;
        std::terminate();
    }
}


void BmpImageSrc::start()
{
    _openFile();
    _getHeaderInfo();
    _getData();
    _inputBuffer->close();
    _inputBuffer.reset();
}
 std::string BmpImageSrc::_create_id()
{
    return "BmpFileSink";
}
std::map<const std::string,std::string> BmpImageSrc::_create_properties()
{
    return {
                    {"location",""},
                    {"name",""},
                    };
}
std::vector<std::string> BmpImageSrc::_create_input_caps()
{
    return {{""}};
}
std::vector<std::string> BmpImageSrc::_create_output_caps()
{
  return {{"image/rgb"}};
}
