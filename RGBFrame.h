
#ifndef RGB2YUV_RGBFRAME_H
#define RGB2YUV_RGBFRAME_H
#include <vector>
#include "memory"
struct RGBPixel
{
    unsigned char R;
    unsigned char G;
    unsigned char B;
};
class RGBFrame
{
public:
    RGBFrame()=default;
    RGBFrame(const unsigned int& bytes,unsigned long w,unsigned long h);
    unsigned int ByteSize();
    std::shared_ptr<std::vector<RGBPixel>>& getPixels();
    friend std::istream& operator>>(std::istream& stream, RGBFrame& rgbFrame);
    RGBPixel operator[](const int i);
    size_t size();
public:
    unsigned long Width;
    unsigned long Height;
private:
    std::shared_ptr<std::vector<RGBPixel>> data;
    unsigned int sizeBytes;
};


#endif
