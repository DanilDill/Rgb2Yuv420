
#include "RgbToYuvFilter.h"
#include <immintrin.h>
unsigned char RgbToYuvFilter::Y(const unsigned char R,const unsigned char G,const unsigned char B)
{

    __m64 R_vector = _m_from_int(R);
    __m64 G_vector = _m_from_int(G);
    __m64 B_vector = _m_from_int(B);
    R_vector = _mm_mullo_pi16(R_vector, _m_from_int(257));
    G_vector = _mm_mul_su32(G_vector, _m_from_int(504));
    B_vector = _mm_mullo_pi16(B_vector, _m_from_int(98));
    __m64 Y_vector = _mm_add_pi32(R_vector,G_vector);
    Y_vector = _mm_add_pi32(Y_vector,B_vector);
    Y_vector = Y_vector >> 10;
    Y_vector = _mm_add_pi8(Y_vector,_m_from_int(16));
    return Y_vector[0];

}

 unsigned char RgbToYuvFilter::U(const unsigned char R,const unsigned char G,const unsigned char B)
{
    __m64 R_vector = _m_from_int(R);
    __m64 G_vector = _m_from_int(G);
    __m64 B_vector = _m_from_int(B);
    R_vector = _mm_mul_su32(R_vector, _m_from_int(439));
    G_vector = _mm_mul_su32(G_vector, _m_from_int(368));
    B_vector = _mm_mul_su32(B_vector, _m_from_int(71));
    __m64 U_vector = _mm_sub_pi32(R_vector ,G_vector);
    U_vector = _mm_add_pi32(U_vector,B_vector);
    U_vector = U_vector >> 10;
    U_vector = _mm_add_pi8(U_vector,_m_from_int(128));
    return U_vector[0];
}

 unsigned char RgbToYuvFilter::V(const unsigned char R,const unsigned char G,const unsigned char B  )
{
    __m64 R_vector = _m_from_int(R);
    __m64 G_vector = _m_from_int(G);
    __m64 B_vector = _m_from_int(B);
    R_vector = _mm_mul_su32(R_vector, _m_from_int(148));
    G_vector = _mm_mul_su32(G_vector, _m_from_int(291));
    B_vector = _mm_mul_su32(B_vector, _m_from_int(439));
    __m64 V_vector = _mm_sub_pi32(B_vector ,G_vector);
    V_vector = _mm_sub_pi32(V_vector,R_vector);
    V_vector = V_vector >> 10;
    V_vector = _mm_add_pi8(V_vector,_m_from_int(128));
    return V_vector[0];

}

std::shared_ptr<std::vector<unsigned char>>  RgbToYuvFilter::Y(const std::shared_ptr<RGBFrame>& frame)
{
    std::shared_ptr<std::vector<unsigned char>> Y = std::make_shared<std::vector<unsigned char>>(std::vector<unsigned char>
                                                                                        (frame->Width*frame->Height));
    for (int i = 0; i < frame->Height; ++i)
    {
        for (int j = 0; j < frame->Width; ++j)
        {
            RGBPixel pixel = (*frame)[i * frame->Width + j];
            Y->at(i * frame->Width + j) = RgbToYuvFilter::Y(pixel.R, pixel.G, pixel.B);
        }
    }
    return Y;
}
std::shared_ptr<std::vector<unsigned char>>   RgbToYuvFilter::U(const std::shared_ptr<RGBFrame>& frame)
{
    std::shared_ptr<std::vector<unsigned char>> U = std::make_shared<std::vector<unsigned char>>(
            std::vector<unsigned char>(frame->Width * frame->Height / 4));

    for (int i = 0; i < frame->Height; i = i + 2)
    {
        for (int j = 0; j < frame->Width; j = j + 2)
        {
            RGBPixel pixel = (*frame)[i * frame->Width + j];
            int uvIndex = (i / 2) * (frame->Width / 2) + (j / 2);
            U->at(uvIndex) = RgbToYuvFilter::U(pixel.R, pixel.G, pixel.B);
        }
    }
    return U;
}

std::shared_ptr<std::vector<unsigned char>>  RgbToYuvFilter::V(const std::shared_ptr<RGBFrame>& frame) {
    std::shared_ptr<std::vector<unsigned char>> V = std::make_shared<std::vector<unsigned char>>(
            std::vector<unsigned char>(frame->Width * frame->Height / 4));

    for (int i = 0; i < frame->Height-1; i = i + 2)
    {
        for (int j = 0; j < frame->Width -1; j = j + 2)
        {
            RGBPixel pixel = (*frame)[i * frame->Width + j];
            int uvIndex = (i / 2) * (frame->Width / 2) + (j / 2);
            V->at(uvIndex) = RgbToYuvFilter::V(pixel.R, pixel.G, pixel.B);
        }

    }
    return V;
}