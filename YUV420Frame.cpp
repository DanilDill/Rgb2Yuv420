
#include "YUV420Frame.h"
#include "RgbToYuvFilter.h"
#include <thread>
#include <future>
#include <fstream>
#include <iostream>

YUV420Frame::YUV420Frame(unsigned int w ,unsigned int h):YUV420Frame()
{
    Width = w;
    Height = h;

}
YUV420Frame::YUV420Frame(YUV420Frame&& other):YUV420Frame( other.Width ,other.Height)
{
    std::swap(Y,other.Y);
    std::swap(U,other.U);
    std::swap(V,other.V);
}

YUV420Frame& YUV420Frame::operator=(YUV420Frame&& other)
{
    std::swap(Width,other.Width);
    std::swap(Height,other.Height);
    std::swap(Y,other.Y);
    std::swap(U,other.U);
    std::swap(V,other.V);
}


YUV420Frame::YUV420Frame(unsigned int w ,unsigned int h, std::istream& stream)
{
    Width = w;
    Height = h;
    Y = std::make_shared<std::vector<unsigned char >>(std::vector<unsigned char >(Width*Height));
    U = std::make_shared<std::vector<unsigned char >>(std::vector<unsigned char >(Width*Height / 4));
    V = std::make_shared<std::vector<unsigned char >>(std::vector<unsigned char >(Width*Height / 4));
    stream >> *this;
}


YUV420Frame::YUV420Frame(const std::shared_ptr<RGBFrame>& rgbFrame,bool async):YUV420Frame(rgbFrame->Width,rgbFrame->Height)
{
    if (async)
    {
        Y = std::move(std::async(std::launch::async,[&](){ return std::move(RgbToYuvFilter::Y(rgbFrame));}).get());
        U = std::move(std::async(std::launch::async,[&](){ return std::move(RgbToYuvFilter::U(rgbFrame));}).get());
        V = std::move(std::async(std::launch::async,[&](){ return std::move(RgbToYuvFilter::V(rgbFrame));}).get());
    }
    else
    {
        Y = std::move(RgbToYuvFilter::Y(rgbFrame));
        U = std::move(RgbToYuvFilter::U(rgbFrame));
        V = std::move(RgbToYuvFilter::V(rgbFrame));
    }

}

std::ofstream& operator<<(std::ofstream& out, YUV420Frame& frame)
{
 // for (int i =  frame.Y->size() -1 ; i >= 0; ++i) {
     //   out << frame.Y->at(i);
   // }
    out.write(reinterpret_cast<char*>(frame.Y->data()),frame.Y->size());
    out.write(reinterpret_cast<char*>(frame.U->data()),frame.U->size());
    out.write(reinterpret_cast<char*>(frame.V->data()),frame.V->size());
   // for (int i =  frame.U->size() -1 ; i >= 0; --i) {
    //    out << frame.U->at(i);
   // }
   // for (int i =  frame.V->size() -1 ; i >= 0; --i) {
   //     out << frame.V->at(i);
   // }

}

std::istream& operator>>(std::istream& stream, YUV420Frame& frame)
{
    stream.read(reinterpret_cast<char*>(frame.Y->data()),frame.Width*frame.Height);
    stream.read(reinterpret_cast<char*>(frame.U->data()),frame.Width*frame.Height/4);
    stream.read(reinterpret_cast<char*>(frame.V->data()),frame.Width*frame.Height/4);
    return stream;
}

YUV420Frame::YUV420Frame(const YUV420Frame& other): YUV420Frame(other.Width ,other.Height, other.Y,other.U,other.V)
{
}

YUV420Frame::YUV420Frame(unsigned int w ,unsigned int h, const std::shared_ptr<std::vector<unsigned char>>& other_Y,
                         const std::shared_ptr<std::vector<unsigned char>>& other_U,
                         const std::shared_ptr<std::vector<unsigned char>>& other_V):
        Y(other_Y),U(other_U),V(other_V),Width(w),Height(h)
{
}

YUV420Frame& YUV420Frame::operator=(const YUV420Frame& other)
{
    Width = other.Width;
    Height = other.Height;
    Y = other.Y;
    U = other.U;
    V = other.V;
    return *this;
}
